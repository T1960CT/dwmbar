#!/bin/bash

#########
# ICONS #
#########
tmp30=
tmp40=
tmp50=
tmp60=
tmp70=

bat20=
bat40=
bat60=
bat80=
bat100=

vol00=
vol30=
vol50=
vol80=

sts_batt=
sts_chrg=

# Disable BIOS fan control
sudo dellfan disable 1

while true; do
	# Peripherals
	sudo pacman -Sy
	updates=$(pacman -Qu | wc -l)
	bright=$(xbacklight -get | cut -d'.' -f1)
	vol=$(awk -F"[][]" '/dB/ { print $2 }' <(amixer -M get Master) | cut -d'%' -f1)
	if [ $vol -le 30 ]; then
		vol_icon=$vol00
	elif [ $vol -gt 80 ]; then
		vol_icon=$vol80
	elif [ $vol -gt 48 ]; then
		vol_icon=$vol50
	elif [ $vol -gt 30 ]; then
		vol_icon=$vol30
	fi

	# Power
	bat_sts=$(cat /sys/class/power_supply/BAT0/status)
	case "$bat_sts" in
    		"Discharging") bat_sts_icon=$sts_batt ;;
    		"Charging") bat_sts_icon=$sts_chrg ;;
    		*) batt_sts_icon="" ;;
	esac

	bat_lvl=$(cat /sys/class/power_supply/BAT0/capacity)
	if [ $bat_lvl -le 20 ]; then
		bat_icon=$bat20
	elif [ $bat_lvl -gt 80 ]; then
		bat_icon=$bat100
	elif [ $bat_lvl -gt 60 ]; then
		bat_icon=$bat80
	elif [ $bat_lvl -gt 40 ]; then
		bat_icon=$bat60
	elif [ $bat_lvl -gt 20 ]; then
		bat_icon=$bat40
	fi

	# Core Components
	cpu_use=$(top -bn1 | grep "Cpu(s)" | sed "s/.*, *\([0-9.]*\)%* id.*/\1/" | awk '{print 100 - $1}' | cut -d'.' -f1)
	if [[ ${#cpu_use} < 2 ]]; then
	    cpu_use="0${cpu_use}"
	fi
	cpu_tmp_raw=$(cat /sys/class/thermal/thermal_zone1/temp)
	cpu_tmp=$((cpu_tmp_raw/1000))
	if [ $cpu_tmp -le 30 ]; then
		temp_icon=$tmp30
		fan_speed=1
	elif [ $cpu_tmp -le 40 ]; then
		temp_icon=$tmp40
		fan_speed=1
	elif [ $cpu_tmp -le 50 ]; then
		tmp_icon=$tmp50
		fan_speed=1
	elif [ $cpu_tmp -le 60 ]; then
		tmp_icon=$tmp60
		fan_speed=2
	elif [ $cpu_tmp -le 70 ]; then
		tmp_icon-$tmp70
		fan_speed=2
	fi

	if [ $(pidof brave| wc -l) -ge 1 ]; then
        	fan_speed=2
	fi

	sudo dellfan $fan_speed > /dev/null

	mem_use=$(free | grep Mem | awk '{print $3/$2 * 100.0}' | cut -d'.' -f1)
	if [[ ${#mem_use} < 2 ]]; then
            mem_use="0${mem_use}"
        fi

	# Networking
	wifi_str=$(nmcli dev wifi | grep -E '^[*]' | awk '/*/ {print $8}')

	# Date and Time
	dt=$(date +"%I:%M%p")

	#ex="^r3,3,14,14^^f10^ ^c#FFFFFF^^b#f7a8b8^ wp box"

	sp="^f5^|^f5^"
	pri="^c#00FF00^"
	sec="^c#FF0000^"
	nrm="^c#DDDDDD^"

	status="^f5^"
	if [ $updates -ge 1 ]; then
		status+="$pri$nrm^f3^$updates"
		status+=$sp
	fi
	status+="$pri$nrm^f3^$bright%"
	status+=$sp
	status+="$pri$vol_icon$nrm^f3^$vol%"
	status+=$sp
	status+="$pri$bat_icon$nrm^f3^$bat_lvl%^f3^$bat_sts_icon"
	status+=$sp
	status+="$pri$nrm^f3^$cpu_use%^f8^$pri$tmp_icon$nrm^f3^$cpu_tmp^f8^^f3^$fan_speed"
	status+=$sp
	status+="$pri$nrm^f3^$mem_use%"
	status+=$sp
	status+="$pri$nrm^f3^$wifi_str"
	status+=$sp
	status+="$pri$nrm^f3^$dt^f5^"

	xsetroot -name "$status"
	sleep 1s
done
